﻿using LapisInvalidStrings.Domain.Models;
using LapisInvalidStrings.Logic.InsertRequests;
using LapisInvalidStrings.Logic.Managers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LapisInvalidStrings.Services.Controllers
{
    [ApiController]
    [Route("invalid-strings")]
    public class InvalidStringController : LapisControllerBase
    {
        private readonly ILogger<IInvalidStringManager> _logger;
        private readonly IInvalidStringManager _manager;

        public InvalidStringController(ILogger<IInvalidStringManager> logger, IInvalidStringManager manager)
        {
            _logger = logger;
            _manager = manager;
        }

        [ProducesResponseType(typeof(IEnumerable<InvalidString>), (int)HttpStatusCode.OK)]
        [Route("all")]
        [HttpGet]
        public async Task<ActionResult> GetInvalidStrings() =>
            await GetResultsAsync(_manager.GetAllInvalidStrings);

        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [Route("is-invalid")]
        [HttpGet]
        public async Task<ActionResult> IsInvalidString(string queryString) =>
            await ExecuteRequestAsync(_manager.IsInvalid, queryString);

        [ProducesResponseType(typeof(IEnumerable<InvalidString>), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.UnprocessableEntity)]
        [HttpPost]
        public async Task<ActionResult> Post(InvalidStringInsertRequest request) =>
            await ExecuteInsertRequestAsync(_manager.AddInvalidStrings, request);

        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [HttpDelete]
        public async Task<ActionResult> Delete(IEnumerable<string> invalidString) =>
            await ExecuteRequestAsync(_manager.DeleteInvalidStrings, invalidString);
    }
}
