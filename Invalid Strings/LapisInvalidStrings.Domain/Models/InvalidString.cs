﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LapisInvalidStrings.Domain.Models
{
    public class InvalidString
    {
        public string Id { get; set; }
        public string Value { get; set; }
        public string AddedBy { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
