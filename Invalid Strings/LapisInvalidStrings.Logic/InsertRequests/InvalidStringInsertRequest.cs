﻿using LapisInvalidStrings.Data.Models.Mongo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LapisInvalidStrings.Logic.InsertRequests
{
    public class InvalidStringInsertRequest : InsertRequestBase
    {
        [Required]
        public IEnumerable<string> InvalidStrings { get; set; }

        [Required]
        public string AddedBy { get; set; }

        //TODO: Add real validation
        [NotMapped]
        public override bool isValid { get => true; }

        public IEnumerable<MongoInvalidString> ToMongoInvalidStrings() =>
            InvalidStrings.Select(x => new MongoInvalidString()
            {
                InvalidString = x,
                AddedBy = AddedBy,
                DateAdded = DateTime.Now
            });

    }        
}
