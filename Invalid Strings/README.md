# lapis-invalid-strings

Invalid string list microservice for Lapis Inventory System

Invalid strings are strings that are not considered valid for Order Numbers or Serial Numbers. This is to prevent accidental scanning of UPCs, Model #s, etc.