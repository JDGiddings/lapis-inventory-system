﻿using LapisRecv.Data.Models.Mongo;
using LapisRecv.Domain;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LapisRecv.Data.Repos
{
    internal class MongoRepo : IMongoRepo
    {
        private readonly MongoClient _dbClient;
        private readonly IMongoDatabase _db;

        public MongoRepo()
        {
            _dbClient = new MongoClient("mongodb://localhost:27017"); //TODO: Fix hardcoded connection string
            _db = _dbClient.GetDatabase("inventory");
        }

        public async Task<TType> GetDocumentAsync<TType>(Expression<Func<TType, bool>> query)
            where TType : MongoDocumentBase
        {
            getCollection<TType>(out var collection);

            var document = collection.Find(query).Limit(1);

            if (document is not null)
            {
                return await document.FirstAsync();
            }
            else
            {
                return default;
            }
        }

        public IMongoQueryable<TType> GetQueryable<TType>()
            where TType : MongoDocumentBase
        {
            getCollection<TType>(out var collection);
            return collection.AsQueryable();
        }

        public async Task<bool> DocumentExists<TType>(Expression<Func<TType, bool>> query)
            where TType : MongoDocumentBase
        {
            getCollection<TType>(out var collection);

            var document = await collection.FindAsync(query);
            return document.FirstOrDefault() is not null;
        }

        public async Task<IEnumerable<TType>> InsertDocumentsAsync<TType>(IEnumerable<TType> documents)
            where TType : MongoDocumentBase
        {
            getCollection<TType>(out var collection);
            await collection.InsertManyAsync(documents);

            return documents;
        }

        private void getCollection<TType>(out IMongoCollection<TType> collection)
            where TType : MongoDocumentBase
        {
            collection = _db.GetCollection<TType>(typeof(TType).Name);
        }
    }

    public interface IMongoRepo
    {
        Task<bool> DocumentExists<TType>(Expression<Func<TType, bool>> query)
            where TType : MongoDocumentBase;
        Task<TType> GetDocumentAsync<TType>(Expression<Func<TType, bool>> query)
            where TType : MongoDocumentBase;
        IMongoQueryable<TType> GetQueryable<TType>()
            where TType : MongoDocumentBase;
        Task<IEnumerable<TType>> InsertDocumentsAsync<TType>(IEnumerable<TType> documents)
            where TType : MongoDocumentBase;
    }
}
