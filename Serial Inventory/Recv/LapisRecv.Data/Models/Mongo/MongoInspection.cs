﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapisRecv.Data.Models.Mongo
{
    public class MongoInspection
    {
        [BsonRequired]
        public bool Passed { get; set; }

        [BsonRequired]
        public string Inspector { get; set; }

        [BsonRequired]
        public DateTime InspectionDate { get; set; }
    }
}
