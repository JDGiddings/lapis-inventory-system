﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LapisRecv.Data.Models.Mongo
{
    public class MongoProduct : MongoDocumentBase
    {
        [BsonRequired]
        public string UPC { get; set; }

        [BsonRequired]
        public string Brand { get; set; }

        [BsonRequired]
        public string Model { get; set; }

        [BsonRequired]
        public string ItemDescription { get; set; }

        [BsonRequired]
        public string SerialType { get; set; }

        [BsonRequired]
        public DateTime CreateDate { get; set; }

        public string SerialPattern { get; set; }
    }
}
