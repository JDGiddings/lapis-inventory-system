﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using LapisRecv.Domain.Models;
using LapisRecv.Logic.InsertRequests;
using LapisRecv.Logic.Managers;

namespace LapisRecv.Service.Controllers
{
    [ApiController]
    [Route("serial-inventory")]
    public class SerialInventoryController : LapisControllerBase
    {
        private readonly ILogger<ISerialRecvManager> _logger;
        private readonly ISerialRecvManager _manager;

        public SerialInventoryController(ILogger<ISerialRecvManager> logger, ISerialRecvManager manager)
        {
            _logger = logger;
            _manager = manager;
        }

        [Route("items")]
        [ProducesResponseType(typeof(IEnumerable<SerialItem>), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.UnprocessableEntity)]
        [HttpPost]
        public async Task<ActionResult> InsertSerialItems([FromBody] SerialRecvInsertRequest items)
        {
            var itemCount = items.Items.Length;

            _logger.LogInformation($"Received receive requiest of {itemCount} items");
            var result = await ExecuteInsertRequestAsync(_manager.InsertSerialItemsAsync, items);

            if ()
            _logger.LogInformation($"Successfully processed receive requiest of {itemCount} items");

            return result;
        }
    }
}
