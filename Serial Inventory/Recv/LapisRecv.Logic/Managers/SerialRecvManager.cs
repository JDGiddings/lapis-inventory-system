﻿using LapisRecv.Data.Repos;
using LapisRecv.Domain.Models;
using LapisRecv.Logic.InsertRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapisRecv.Logic.Managers
{
    internal class SerialRecvManager : ISerialRecvManager
    {
        private readonly IMongoRepo _repo;

        public SerialRecvManager(IMongoRepo repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<SerialItem>> InsertSerialItemsAsync(SerialRecvInsertRequest requests)
        {
            var documents = await _repo.InsertDocumentsAsync(await Task.WhenAll(requests.Items.Select(x => x.ToMongoSerialItem(_repo))));
            return await Task.WhenAll(documents.Select(x => x.ToSerialItem(_repo)));
        }
    }

    public interface ISerialRecvManager
    {
        Task<IEnumerable<SerialItem>> InsertSerialItemsAsync(SerialRecvInsertRequest requests);
    }
}
