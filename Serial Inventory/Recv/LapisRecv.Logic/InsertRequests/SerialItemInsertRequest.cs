﻿using LapisRecv.Data.Models.Mongo;
using LapisRecv.Data.Repos;
using LapisRecv.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LapisRecv.Logic.InsertRequests
{
    public class SerialItemInsertRequest : InsertRequestBase
    {
        //private readonly IMongoRepo _repo;

        //SerialItemInsertRequest(IMongoRepo repo)
        //{
        //    _repo = repo;
        //}

        [Required]
        public string SerialNumber { get; set; }

        [Required]
        public string ItemCondition { get; set; }

        [Required]
        [RegularExpression("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]")]
        public string UPC { get; set; }

        public ItemNotesInsertRequest[] Notes { get; set; }

        //TODO: Get real validation
        [NotMapped]
        public override bool isValid
        {
            get => true;
        }

        //private async Task<bool> Validate() =>
        //    await _repo.DocumentExists<MongoProduct>(x => x.UPC == UPC) &&                          //Product Exists
        //    !await _repo.DocumentExists<MongoSerialItem>(x => x.SerialNumber == SerialNumber) &&    //Serial Number does not exist
        //    await _repo.DocumentExists<MongoItemCondition>(x => x.Condition == ItemCondition);      //Condition exists

        public async Task<MongoSerialItem> ToMongoSerialItem(IMongoRepo repo) =>
            new MongoSerialItem()
            {
                SerialNumber = SerialNumber,

                Notes = Notes.Select(x => x.ToMongoItemNotes()).ToArray(),

                ProductId = await Task.Run(() => repo.GetQueryable<MongoProduct>()
                                                      .Where(x => x.UPC == UPC)
                                                      .Select(x => x.Id)
                                                      .FirstOrDefault()),

                ItemConditionId = await Task.Run(() => repo.GetQueryable<MongoItemCondition>()
                                                            .Where(x => x.Condition == ItemCondition)
                                                            .Select(x => x.Id)
                                                            .FirstOrDefault()),

                ReceiveDate = DateTime.Now,
                Cost = null,
                Inspections = Array.Empty<Inspection>(),
                IsScannedOut = false,
                OrderNumber = null,
                ScanOutDate = null
            };
    }
}
