﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LapisRecv.Domain.Models
{
    public class ItemNotes
    {
        public string Author { get; set; }
        public string Notes { get; set; }
        public DateTime WrittenDate { get; set; }
    }
}
