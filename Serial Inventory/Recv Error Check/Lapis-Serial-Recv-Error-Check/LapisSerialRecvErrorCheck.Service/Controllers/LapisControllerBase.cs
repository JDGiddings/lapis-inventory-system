﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LapisSerialRecvErrorCheck.Services.Controllers
{
    public abstract class LapisControllerBase : ControllerBase
    {
        public async Task<ActionResult> ExecuteRequestAsync<TIn, TResponse>(
           Func<TIn, Task<TResponse>> exec,
           TIn model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await exec(model);

            if (result is null)
            {
                return NotFound(model);
            }

            return Ok(result); 
        }   
    }
}
