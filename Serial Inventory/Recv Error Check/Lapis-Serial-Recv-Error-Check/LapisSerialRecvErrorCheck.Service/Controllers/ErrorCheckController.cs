﻿using LapisSerialRecvErrorCheck.Logic.Models.InputModels;
using LapisSerialRecvErrorCheck.Logic.Models.ResponseModels;
using LapisSerialRecvErrorCheck.Logic.Managers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LapisSerialRecvErrorCheck.Services.Controllers
{
    [ApiController]
    [Route("error-check")]
    public class ErrorCheckController : LapisControllerBase
    {
        private readonly ILogger<IErrorCheckManager> _logger;
        private readonly IErrorCheckManager _manager;

        public ErrorCheckController(ILogger<IErrorCheckManager> logger, IErrorCheckManager manager)
        {
            _logger = logger;
            _manager = manager;
        }

        [Route("receive-serial-item")]
        [ProducesResponseType(typeof(ErrorMsgResponse), (int)HttpStatusCode.OK)]
        [HttpGet]
        public async Task<ActionResult> RecvSerialItem([FromBody] SerialRecvErrorCheckInput input) =>
            await ExecuteRequestAsync(_manager.ReceiveSerialItemErrorCheck, input);
    }
}
