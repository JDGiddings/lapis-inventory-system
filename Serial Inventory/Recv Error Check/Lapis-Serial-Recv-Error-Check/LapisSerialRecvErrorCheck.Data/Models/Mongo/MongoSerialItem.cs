﻿using LapisSerialRecvErrorCheck.Domain.Models;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LapisSerialRecvErrorCheck.Data.Models.Mongo
{
    public class MongoSerialItem : MongoDocumentBase
    {
        [BsonRequired]
        public string SerialNumber { get; set; }

        [BsonRequired]
        public string ProductId { get; set; }

        [BsonRequired]
        public DateTime ReceiveDate { get; set; }

        [BsonRequired]
        public string ItemConditionId { get; set; }

        public ItemCost Cost { get; set; }

        public ItemNotes[] Notes { get; set; }

        public Inspection[] Inspections { get; set; }

        [BsonRequired]
        public bool IsScannedOut { get; set; }

        public string OrderNumber { get; set; }

        public DateTime? ScanOutDate { get; set; }
    }
}
