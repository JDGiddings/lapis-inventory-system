﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LapisSerialRecvErrorCheck.Domain.Models
{
    public class ItemCondition
    {
        public string Id { get; set; }
        public string Condition { get; set; }
        public bool RequireInspection { get; set; }
        public bool PrimaryInventory { get; set; }
    }
}
