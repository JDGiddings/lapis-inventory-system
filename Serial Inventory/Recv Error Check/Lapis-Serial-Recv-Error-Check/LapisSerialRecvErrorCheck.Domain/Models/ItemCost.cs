﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LapisSerialRecvErrorCheck.Domain.Models
{
    public class ItemCost
    {
        [Required]
        public double Amount { get; set; }

        [Required]
        public string Currency { get; set; }
    }
}
