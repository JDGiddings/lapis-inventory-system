﻿using LapisSerialRecvErrorCheck.Data.Models.Mongo;
using LapisSerialRecvErrorCheck.Domain.Models;
using LapisSerialRecvErrorCheck.Data.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace LapisSerialRecvErrorCheck.Logic
{
    public static class MongoConversionExtensionMethods
    {
        public static async Task<SerialItem> ToSerialItem(this MongoSerialItem mongoDoc, IMongoRepo _repo)
        {
            var mongoProduct = await _repo.GetDocumentAsync<MongoProduct>(x => x.Id == mongoDoc.ProductId);
            var mongoItemCondition = await _repo.GetDocumentAsync<MongoItemCondition>(x => x.Id == mongoDoc.ItemConditionId);

            return new SerialItem()
            {
                Id = mongoDoc.Id,
                SerialNumber = mongoDoc.SerialNumber,
                Notes = mongoDoc.Notes,
                ReceiveDate = mongoDoc.ReceiveDate,
                Inspections = mongoDoc.Inspections,
                IsScannedOut = mongoDoc.IsScannedOut,
                OrderNumber = mongoDoc.OrderNumber,
                ScanOutDate = mongoDoc.ScanOutDate,
                Cost = mongoDoc.Cost,

                Product = mongoProduct.ToProduct(),
                ItemCondition = mongoItemCondition.ToItemCondition()
            };
        }

        public static ItemCondition ToItemCondition(this MongoItemCondition mongoDoc) =>
           new ItemCondition()
            {
                Id = mongoDoc.Id,
                Condition = mongoDoc.Condition,
                RequireInspection = mongoDoc.RequireInspection,
                PrimaryInventory = mongoDoc.PrimaryInventory
            };

        public static Product ToProduct(this MongoProduct mongoDoc) =>
            new Product()
            {
                Id = mongoDoc.Id,
                UPC = mongoDoc.UPC,
                Brand = mongoDoc.Brand,
                Model = mongoDoc.Model,
                ItemDescription = mongoDoc.ItemDescription,
                SerialType = mongoDoc.SerialType,
                CreateDate = mongoDoc.CreateDate,
                SerialPattern = mongoDoc.SerialPattern
            };
    }
}
