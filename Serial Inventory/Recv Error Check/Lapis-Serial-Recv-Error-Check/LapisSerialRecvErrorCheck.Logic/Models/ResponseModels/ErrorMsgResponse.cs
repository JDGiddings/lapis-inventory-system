﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LapisSerialRecvErrorCheck.Logic.Models.ResponseModels
{
    public class ErrorMsgResponse
    {
        private List<string> _errorMsgs = new List<string>();
        private bool _playSound = false;
        private bool _preventProcess = false;
        private bool _requirePassword = false;

        public List<string> ErrorMsgs
        {
            get => _errorMsgs;
        }

        public void Add(string errorMsg)
        {
            _errorMsgs.Add(errorMsg);
        }

        //Values can be set to True one time, then cannot be set to false afterwards
        //This prevents accidental overwriting of flags
        public bool PlaySound
        {
            get => _playSound;

            set
            {
                if (value)
                {
                    _playSound = true;
                }
                else
                {
                    throw new InvalidOperationException("Flag cannot be set to false once it has been set to true");
                }
            }
        }
        public bool PreventProcess
        {
            get => _preventProcess;

            set
            {
                if (value)
                {
                    _preventProcess = true;
                }
                else
                {
                    throw new InvalidOperationException("Flag cannot be set to false once it has been set to true");
                }
            }
        }

        public bool RequirePassword
        {
            get => _requirePassword;

            set
            {
                if (value)
                {
                    _requirePassword = true;
                }
                else
                {
                    throw new InvalidOperationException("Flag cannot be set to false once it has been set to true");
                }
            }
        }
    }
}
