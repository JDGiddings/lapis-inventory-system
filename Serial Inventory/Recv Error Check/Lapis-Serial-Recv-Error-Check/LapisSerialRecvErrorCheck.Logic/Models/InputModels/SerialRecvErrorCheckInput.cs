﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LapisSerialRecvErrorCheck.Logic.Models.InputModels
{
    public class SerialRecvErrorCheckInput
    {
        public IEnumerable<ScanInfo> ScanInfo { get; set; }
    }
}
