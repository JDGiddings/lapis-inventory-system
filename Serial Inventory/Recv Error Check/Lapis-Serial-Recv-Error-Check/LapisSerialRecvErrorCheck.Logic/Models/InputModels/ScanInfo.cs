﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapisSerialRecvErrorCheck.Logic.Models.InputModels
{
    public class ScanInfo
    {
        public string SerialNumber { get; set; }
        public string UPC { get; set; }
    }
}
