﻿using LapisSerialRecvErrorCheck.Data.Models.Mongo;
using LapisSerialRecvErrorCheck.Data.Repos;
using LapisSerialRecvErrorCheck.Logic.Models.InputModels;
using LapisSerialRecvErrorCheck.Logic.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using LapisSerialRecvErrorCheck.Domain.Models;

[assembly: InternalsVisibleTo("LapisSerialRecvErrorCheck.Tests")]
namespace LapisSerialRecvErrorCheck.Logic.Managers
{
    internal class ErrorCheckManager : IErrorCheckManager
    {
        private readonly IMongoRepo _mongoRepo;
        //private readonly IInvalidStringManager _invalidStringManager;

        public ErrorCheckManager(IMongoRepo mongoRepo)
        {
            _mongoRepo = mongoRepo;
            //_invalidStringManager = invalidStringManager;
        }

        public async Task<ErrorMsgResponse> ReceiveSerialItemErrorCheck(SerialRecvErrorCheckInput input)
        {
            var response = new ErrorMsgResponse();

            var results = input.ScanInfo.Select(x => ErrorCheck(x, ref response));

            return response;
        }

        private ErrorMsgResponse ErrorCheck(ScanInfo input, ref ErrorMsgResponse response)
        {
            var mongoProduct = await _mongoRepo.GetDocumentAsync<MongoProduct>(x => x.UPC == input.UPC);
            Product product = null;

            //Verify Product
            if (string.IsNullOrWhiteSpace(input.UPC))
            {
                response.Add("UPC is Null");
                response.PreventProcess = true;
            }
            else if (mongoProduct == default)
            {
                response.Add("Unrecognized UPC");
                response.PlaySound = true;
                response.PreventProcess = true;
            }
            else if (mongoProduct.SerialType != "Serial")
            {
                response.Add("Not a Serial Item");
                response.PlaySound = true;
                response.PreventProcess = true;
            }
            else
            {
                //Product is verified
                product = mongoProduct.ToProduct();
            }

            //Verify Item
            if (string.IsNullOrWhiteSpace(input.SerialNumber))
            {
                response.Add("Serial Number is Null");
                response.PreventProcess = true;
            }
            //else if (_invalidStringManager.IsInvalid(input.SerialNumber))
            //{
            //    response.Add("Invalid Serial Number");
            //    response.PlaySound = true;
            //    response.PreventProcess = true;
            //}
            else if (await _mongoRepo.DocumentExists<MongoSerialItem>(x => x.SerialNumber == input.SerialNumber))
            {
                response.Add("Item Already Received");
                response.PlaySound = true;
                response.PreventProcess = true;
            }
            else
            {
                
                //Only check these if the above is already passed
                if (input.SerialNumber.Length >= 50)
                {
                    response.Add("Serial Number too Long");
                    response.PlaySound = true;
                    response.PreventProcess = true;
                }

                //if (product is not null 
                //    && product.SerialPattern is not null 
                //    && !product.SerialPattern.Match(input.SerialNumber).Success)
                //{
                //    response.Add("Serial Number does not Match Pattern");
                //    response.PlaySound = true;
                //    response.PreventProcess = true;
                //}
            }

            return response;
        }
    }

    public interface IErrorCheckManager
    {
        Task<ErrorMsgResponse> ReceiveSerialItemErrorCheck(SerialRecvErrorCheckInput input);
    }
}
