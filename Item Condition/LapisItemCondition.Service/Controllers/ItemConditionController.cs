﻿using LapisItemCondition.Domain.Models;
using LapisItemCondition.Logic.InsertRequests;
using LapisItemCondition.Logic.Managers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LapisItemCondition.Services.Controllers
{
    [ApiController]
    [Route("item-condition")]
    public class ItemConditionController : LapisControllerBase
    {
        private readonly ILogger<IItemConditionManager> _logger;
        private readonly IItemConditionManager _manager;

        public ItemConditionController(ILogger<IItemConditionManager> logger, IItemConditionManager manager)
        {
            _logger = logger;
            _manager = manager;
        }

        [ProducesResponseType(typeof(ItemCondition), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [HttpGet]
        public async Task<ActionResult> GetItemCondition(string condition) =>
            await ExecuteRequestAsync(_manager.GetItemConditionAsync, condition);

        [Route("all")]
        [ProducesResponseType(typeof(IEnumerable<ItemCondition>), (int)HttpStatusCode.OK)]
        [HttpGet]
        public async Task<ActionResult> GetAllItemConditions() =>
            await GetResultsAsync(_manager.GetItemConditionsAsync);

        [ProducesResponseType(typeof(ItemCondition), (int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.UnprocessableEntity)]
        [HttpPost]
        public async Task<ActionResult> InsertItemCondition([FromBody] ItemConditionInsertRequest item) =>
            await ExecuteInsertRequestAsync(_manager.InsertItemConditionAsync, item);
    }
}
