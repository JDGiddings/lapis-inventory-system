﻿using LapisItemCondition.Data.Repos;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapisItemCondition.Data
{
    public static class Startup
    {
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<IMongoRepo, MongoRepo>();
        }
    }
}
