﻿using LapisItemCondition.Data.Repos;
using LapisItemCondition.Data.Models.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LapisItemCondition.Domain.Models;
using LapisItemCondition.Logic.InsertRequests;

namespace LapisItemCondition.Logic.Managers
{
    internal class ItemConditionManager : IItemConditionManager
    {
        private readonly IMongoRepo _repo;

        public ItemConditionManager(IMongoRepo repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<ItemCondition>> GetItemConditionsAsync()
        {
            var documents = await _repo.GetDocumentsAsync<MongoItemCondition>(x => true);
            return documents.Select(x => x.ToItemCondition());
        }

        public async Task<ItemCondition> GetItemConditionAsync(string cond)
        {
            var document = await _repo.GetDocumentAsync<MongoItemCondition>(x => x.Condition == cond);
            return document.ToItemCondition();
        }

        public async Task<ItemCondition> InsertItemConditionAsync(ItemConditionInsertRequest item)
        {
            var document = await _repo.InsertDocumentAsync(item.ToMongoItemCondition());
            return document.ToItemCondition();
        }
            
    }

    public interface IItemConditionManager
    {
        Task<IEnumerable<ItemCondition>> GetItemConditionsAsync();
        Task<ItemCondition> GetItemConditionAsync(string cond);
        Task<ItemCondition> InsertItemConditionAsync(ItemConditionInsertRequest item);
    }
}
