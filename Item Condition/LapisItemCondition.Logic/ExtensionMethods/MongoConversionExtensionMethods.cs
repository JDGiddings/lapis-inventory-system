﻿using LapisItemCondition.Data.Models.Mongo;
using LapisItemCondition.Domain.Models;
using LapisItemCondition.Data.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace LapisItemCondition.Logic
{
    public static class MongoConversionExtensionMethods
    {
        public static ItemCondition ToItemCondition(this MongoItemCondition mongoDoc) =>
           new ItemCondition()
            {
                Id = mongoDoc.Id,
                Condition = mongoDoc.Condition,
                RequireInspection = mongoDoc.RequireInspection,
                PrimaryInventory = mongoDoc.PrimaryInventory
            };
    }
}
