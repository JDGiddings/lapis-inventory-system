﻿using LapisItemCondition.Logic.Managers;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapisItemCondition.Logic
{
    public static class Startup
    {
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<IItemConditionManager, ItemConditionManager>();

            Data.Startup.Register(services);
        }
    }
}
