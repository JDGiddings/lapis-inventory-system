﻿using LapisItemCondition.Data.Models.Mongo;
using LapisItemCondition.Data.Repos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LapisItemCondition.Logic.InsertRequests
{
    public class ItemConditionInsertRequest : InsertRequestBase
    {
        [Required]
        public string Condition { get; set; }

        [Required]
        public bool RequireInspection { get; set; }

        [Required]
        public bool PrimaryInventory { get; set; }

        [NotMapped]
        public override bool isValid { get => true; } //TODO: Get real data validation

        public MongoItemCondition ToMongoItemCondition() =>
            new MongoItemCondition()
            {
                Condition = Condition,
                RequireInspection = RequireInspection,
                PrimaryInventory = PrimaryInventory
            };
    }
}
